package test;

import java.io.*;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class InputToList {
	
	 private List<Integer> getListOfIp() throws IOException {

         int min = 0;
         int max = 0;

		 try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){

			 System.out.print("Enter start of IP range: ");
			 min = Integer.parseInt(br.readLine());
			 
			 System.out.print("Enter end of IP range: ");
			 max = Integer.parseInt(br.readLine());
			 System.out.println();
			 
		 }catch(NumberFormatException e) {

			 System.out.println("You did not enter start or end of ip range! The programm is stopped" );

		 }


		 return rangeOfIp(min, max);
	 }

	private static List<Integer> rangeOfIp(int min, int max){

		List<Integer> list = new ArrayList<>();

		for (int i = min; i < max + 1; i++) {

			list.add(i);

		}

		return list;

	}

	
	 public void printTemperaturesListOfServers() throws IOException {	
		 
		List<Integer> listOfIp = getListOfIp();
		
		for (int i = 0; i < listOfIp.size(); i++) {
			
			Integer ip = listOfIp.get(i);
			String temperature = getTemperatureByIp(ip);
			
			if (temperature != null) {
				
				double statusTemp = Double.parseDouble(temperature);
				String status;
				
				if(statusTemp >= 80.0) {
					
					status = "VERY HOT!";
					
				}else if(statusTemp >= 70.0) {
					
					status = "HOT!";
					
				}else {
					
					status = "Normal";
					
				}
			
			System.out.println("# "+ ip +" - temperature = " + getTemperatureByIp(ip) + " - " + status);
			System.out.println();
			
			
			}
			 
		}
				
	}
	
	private String getTemperatureByIp(Integer ip) throws IOException {
		
		Document doc = null;
		String page = "/sys_inf.php";
        String subnet = "127";

		try {

            doc = Jsoup.connect("http://192.168." + subnet + "." + ip + page).timeout(20000).get();
			
		}catch (SocketTimeoutException e) {
			
			//e.printStackTrace();
			
			System.out.println("# "+ ip + " - no connection!");
			System.out.println();
			
		}catch(HttpStatusException e) {
			
			System.out.println("# "+ ip + " - Brickstream");
			System.out.println();
			
		}
		
		if (doc != null) {
			
			Elements body = doc.select("body"); 
	    	
		    Elements temp = body.select("tr:contains(Temperature)"); 
		    
		    Elements temp2 = temp.select("td:matchesOwn(\\d)");
			
		    String temp3 = temp2.toString().replaceAll("<[^>]+>", "");
			
		    return temp3;
		    
		  }  
		
		return null;
					
	}
	
}	
	

	

	


